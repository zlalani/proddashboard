/*********************************************************************************************************************************
* Author : Faisal Zia <faisal.zia@jintech.com>
* Date : 05/11/2018
* Description : create prod_logs table
* Jira Ticket : https://oliveruk.atlassian.net/browse/OMG-4586
***********************************************************************************************************************************/


-- Table: prod_logs

DROP TABLE IF EXISTS prod_logs;

CREATE TABLE prod_logs
(
    id serial,
    log_time timestamp with time zone,
    activity character varying(80),
    prod_server_id character varying,
    message text,
    CONSTRAINT prod_logs_pkey PRIMARY KEY (id)
)
WITH (
    OIDS=FALSE
);
