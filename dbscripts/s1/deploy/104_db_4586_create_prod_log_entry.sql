/********************************************************************************************
* Author : Faisal Zia<faisal.zia@jintech.com>
* Date : 05/11/2018
* Description : Create a log entry in otp log table
* Jira Ticket : https://oliveruk.atlassian.net/browse/OMG-4586
*********************************************************************************************/

-- Function: create_prod_log_entry(character varying, character varying, text)

DROP FUNCTION IF EXISTS create_prod_log_entry(character varying, character varying, text);

CREATE OR REPLACE FUNCTION create_prod_log_entry(
    prodserver_id character varying, 
    log_activity character varying, 
    log_msg text)
  RETURNS integer AS
$BODY$
    DECLARE
        lastid int;
BEGIN
    -- insert a new record into prod_logs
    INSERT INTO "public"."prod_logs" (prod_server_id, activity, message, log_time)
        VALUES (prodserver_id, log_activity, log_msg, now()) RETURNING id INTO lastid;

    RETURN lastid;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  