/********************************************************************************************
* Author : Faisal Zia<faisal.zia@jintech.com
* Date : 02/11/2018
* Description : Create production servers table
* Jira Ticket : https://oliveruk.atlassian.net/browse/OMG-4586
*********************************************************************************************/

-- Table: public.prod_servers

DROP TABLE IF EXISTS public.prod_servers;

CREATE TABLE public.prod_servers
(
  id serial,
  server_id character varying(255),
  status integer, -- defines Servers RAG status: 1:Green, 2:Amber, 3:Red
  last_active timestamp with time zone,
  CONSTRAINT prod_servers_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

COMMENT ON COLUMN public.prod_servers.status IS 'defines Servers RAG status: 1:Green, 2:Amber, 3:Red';
