/********************************************************************************************
* Author : Faisal Zia<faisal.zia@jintech.com
* Date : 02/11/2018
* Description : Register production servers
* Jira Ticket : https://oliveruk.atlassian.net/browse/OMG-4586
*********************************************************************************************/

-- Function: register_prod_servers(character varying, integer)

DROP FUNCTION IF EXISTS register_prod_servers(character varying, integer);

CREATE OR REPLACE FUNCTION register_prod_servers(
    server_num character varying, 
    server_status integer)
  RETURNS character varying AS
$BODY$
    DECLARE
        lastid character varying;
BEGIN
    -- insert a new record into prod_servers
    WITH upsert AS (UPDATE "public"."prod_servers"
        SET
            status = server_status,
            last_active = 
            CASE WHEN server_status = 1
                THEN now()
            ELSE last_active
            END
        WHERE server_id = server_num 
        RETURNING server_id)
    INSERT INTO "public"."prod_servers" ( server_id, last_active, status )
        SELECT  server_num, now(), server_status 
        WHERE NOT EXISTS (SELECT 1 FROM upsert WHERE server_id = server_num)
        RETURNING server_id INTO lastid;
    IF lastid IS NULL THEN
        lastid := server_num;
    END IF;

    RETURN lastid;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  