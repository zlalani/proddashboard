/********************************************************************************************
* Author : Faisal Zia<faisal.zia@jintech.com
* Date : 02/11/2018
* Description : Get list of all production servers
* Jira Ticket : https://oliveruk.atlassian.net/browse/OMG-4586
*********************************************************************************************/

-- Function: get_all_prod_servers()

DROP FUNCTION IF EXISTS get_all_prod_servers();

CREATE OR REPLACE FUNCTION get_all_prod_servers()
  RETURNS TABLE(
  id integer,
  server_number character varying,
  last_active double precision, 
  status integer
) AS
$BODY$
BEGIN
   RETURN QUERY 
    SELECT 
        prodser.id,
        prodser.server_id,
        floor(EXTRACT(EPOCH FROM prodser.last_active)), 
        prodser.status
    FROM "public"."prod_servers" AS prodser;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;