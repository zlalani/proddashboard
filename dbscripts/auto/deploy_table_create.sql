
/************************************************************
* Author : Geetha Nair<Geethanair@oliver-marketing.com
* Date : 28/April/2015
* Description : Create a table called deploy that holds details of db scripts run on master
*************************************************************/


-- Table: "deploy"

-- DROP TABLE "deploy";

CREATE TABLE "deploy"
(
  "id" serial NOT NULL,
  "sprint_id" integer,
  "script_id" integer,
  "status" boolean,
  CONSTRAINT "deploy_pkey" PRIMARY KEY ("id")
)
WITH (
  OIDS=FALSE
);