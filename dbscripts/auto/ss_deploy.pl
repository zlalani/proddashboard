#!/usr/bin/perl

###############################################################################
#
#             DB deployment automation(support scripts)
#             must always be run from the target auto folder.
#
#             always backup DB before a deploy
#             pg_dump dashboard -Fc -U postgres -h localhost > dashboard_backup.sql
###############################################################################

use DBI;
use Data::Dumper;
use strict;

my $C_PGEXEC = 'PGPASSWORD=oliver99 psql -U postgres -d proddashboard -h localhost -1 -f ';
my $C_LOG = "pg_log.txt";

###############################################################################
#         writeLog
###############################################################################

sub writeLog {
    my ($status, $cmd, $output) = @_;

    open(LOGFILE,">>$C_LOG") or die("Can't open log file.\n");
    print LOGFILE ("$status RUNNING $cmd\n");
    print LOGFILE ("$output\n");
    print LOGFILE ("--------------------------------------------------------------------------------------\n");
    close(LOGFILE);
}


###############################################################################
#         updateDeploy
#         add completed deployment item to tracking db
###############################################################################
sub updateDeploy{
     my ($dbh, $file) = @_;

     my $cmd = $C_PGEXEC.$file;

     my $log = `$cmd`;
     if ( $? == -1 )
     {
          writeLog("FAILED", $cmd, $log);
          print "command $cmd failed: $!\n";
          exit(0);
     }
     else{
          writeLog("OK", $cmd, $log);
          print $log.'\n';
     }
}


###############################################################################
#         deploy_next
#         deploy all incomplete items from specified item + 1 within this
#         sprint only.
###############################################################################
sub deploy_next{
     my ($dbh, $item) = @_;
     print "Beginning deployment at item $item \n";

     my $spath = "../supportscripts/".$item."_*.sql";
     print "checking $spath \n";

     my @files = glob($spath);
     my $numfiles = @files;

     if ( $numfiles ){
          print "found deploy file for item $item as $files[0] \n";
          updateDeploy( $dbh, $files[0] );
     }
     else{
          print "no more items\n";
          return -1;
     }

}

sub main{
     print "Begin Support Deploy\n";

     my $dbh = DBI->connect('dbi:Pg:dbname=proddashboard;host=localhost','pgdev','pgdev',{AutoCommit=>1,RaiseError=>1,PrintError=>0});

     my $cont = 1;
     my $item = 1;
     my $res1 = 0;
     do{
          # run 
          $res1 = deploy_next( $dbh, $item );

          # move to next item
          $item++;

          # now check if this completed the deploy or more sprints left
          if ($res1 == -1){
               $cont = 0;
          }
     } while ($cont);

     print "End Support Deploy\n";
}


#
# run main
#
main();

# end