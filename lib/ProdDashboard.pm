package ProdDashboard;
use Dancer2;
use Dancer2::Plugin::Ajax;
use homepage::homepage_controller;
use production::production_controller;

our $VERSION = '0.1';

prefix "/";

#################################################################
# Route Home page
#################################################################
get '/' => \&homepage_controller::showHomepage;

########## API ROUTES ###########################################
post '/api/registerServer' => \&production_controller::registerServers;
post '/api/updateServer' => \&production_controller::updateServers;
get '/api/ragupdate' => \&production_controller::ragUpdate;

true;
