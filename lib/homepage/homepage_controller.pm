package homepage_controller;

use Template;
use Dancer2 appname => 'ProdDashboard';
use Data::Dumper;
use strict;
use warnings;
use TryCatch;
use production::production_helper;
use DateTime;
use DateTime::Format::Strptime;

use utils::ajax::response_helper;
use POSIX ();

sub showHomepage {
	my $error;
	try {
		my $servers_list = production_helper::getAllProdServers();
		my $local_domain = config->{domain};

		return template 'homepage/homepage', {
			servers_list => $servers_list, 
			domain => $local_domain
		};
	} catch ($error){
		die $error;
	};
}

1;
