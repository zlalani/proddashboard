package utils::support::action_helper;

use Dancer2 appname => 'ProdDashboard';
use Carp;
use TryCatch;
use Scalar::Util qw(blessed);
use Moo;

use Dancer2::Plugin::Deferred;
use utils::ajax::response_helper;
use utils::support::WhiteListSanitizer;

use Exporter qw(import);
our @EXPORT = qw(
	redirect_to
	sanitize
	handleException
);
our @EXPORT_OK = qw();

sub  sanitize {
	return WhiteListSanitizer->new->sanitize(@_);
}

sub redirect_to {
	my ($path, %args) = @_;

	while (my ($key, $value) = each %args) {
		deferred $key => $value;
	}

	return redirect $path;
}

sub handleException{
	my ($error, $target) = @_;
 
	my $msg;

	# check fo Moo based error objects
	if (blessed $error){

		if ( $error->isa('errors::AccessDenied') or $error->isa('errors::ActivityDenied') ) {
			$msg = 'Access to this resource has been denied.';
		}
		elsif ($error->isa('errors::dbError') and ($error->type() eq 'UniqueViolation')) {
			$msg =  "Record already exists."
		}
		elsif ($error->isa('errors::dbError')) {
			$msg = "Your data was not saved successfully! Please try again later."
		}
		else{
			$msg = $error->{message};
		}

		if(defined $error->{target}){
			$target = $error->{target};
		}
	}
	else{

		$msg = 'Something went wrong.';
	}
	if(defined $target && $target ne ""){
		deferred error => $msg;
		redirect $target;
	}
	else{
		return ajax_error_response(
			message => $msg
		);
	}
}

1;
