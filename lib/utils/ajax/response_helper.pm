package utils::ajax::response_helper;

use strict;
use warnings;
use JSON qw(to_json);

use Exporter qw(import);
our @EXPORT = qw(ajax_error_response ajax_data_response);

sub ajax_error_response {
    return ajax_json_response('error', {@_});
}

sub ajax_data_response {
    return ajax_json_response('data', {@_});
}

sub ajax_json_response {
    my ($root, $data) = @_;
    return to_json({($root => $data)}, { allow_blessed => 1, convert_blessed => 1 });
}

1;
