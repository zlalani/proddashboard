package Mailer;

use Net::SMTP;
use Authen::SASL;
use Time::Local;
use Data::Dumper;

######################################################################
#	send_mail
#
#	param 1 - from address
#	param 2 - either to address, or array of to addresses (use , to to multi)
#	param 3	- body of mail
#	param 4 - subject
######################################################################
 sub send_mail
    {
        my ($from, $to, $body, $subject, $msg);
        my ($SMTP_HOST, $smtp);
        my (@to_addr);

        $from = $_[0];
        $to = $_[1];
        $body = $_[2];
        $subject = $_[3];
        
        $SMTP_HOST = 'smtp.mailgun.org';
        
        # convert the list of to address to array
        @to_addr = split(',', $to);

        $msg = "MIME-Version: 1.0\n"
             . "From: $from\n"
             . "To: " . join(';', @to_addr) . "\n"
             . "Subject: $subject\n\n"      # Double \n
             . $body;

        #
        # Open a SMTP session
        #
        $smtp = Net::SMTP->new( $SMTP_HOST,
			Hello => 'oliver.solutions',
            Debug => 0,       # Change to a 1 to turn on debug messages
        );
        
        if(!defined($smtp) || !($smtp))
        {
            print "SMTP ERROR: Unable to open smtp session.\n";

            return 0;
        }

        $smtp->auth('postmaster@oliver.solutions', 'a989aff7fdb39352a0b7b6e3ee4794ed');
        $smtp->banner();
        $smtp->domain();

        #
        # Pass the 'from' email address, exit if error
        #
        if (! ($smtp->mail( $from ) ) )
        {
            return 0;
        }

        #
        # Pass the recipient address(es)
        #
        foreach (@to_addr) {
	        if (! ($smtp->recipient( $_ )))
	        {
	            return 0;
	        }
        }

        #
        # Send the message
        #
        $smtp->data( $msg );

        $smtp->quit;
        
        return 1;
    }

# must have this at the end for a require
1;

