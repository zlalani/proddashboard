#!/usr/bin/perl

use Dancer2;
use strict;
use LWP::UserAgent;
use Data::Dumper;
use JSON qw(decode_json encode_json);

my %daemon_config;

if ( -e 'daemon_config.pl'){
	%daemon_config = do 'daemon_config.pl';	
}

sub main{
	my $dashboard_url;

	if(%daemon_config) {
		$dashboard_url = $daemon_config{destination_ip};
	}else{
		$dashboard_url = 'localhost';
	}
	
	my $http_client = LWP::UserAgent->new;
	my $http_response = $http_client->get("$dashboard_url/api/ragupdate");

	if ($http_response->is_success) {
		my $response = decode_json($http_response->decoded_content) ;
		if(defined $response->{error}){
			print Dumper $response->{error}->{message};
		}
	}
}

main();

1;
