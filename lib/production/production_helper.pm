package production_helper;

use Template;
use Dancer2 appname => 'ProdDashboard';
use Data::Dumper;
use TryCatch;
use production::dao::production_db;
use email::email_helper;
use JSON qw(encode_json decode_json);
use strict;
use warnings;

sub registerServer {
	my ($server_id, $status) = @_;

	my $res = production_db::registerServer($server_id, $status);
	
	return 1;
}

sub getAllProdServers {
	my $servers_list = production_db::getAllProdServers();
	
	return $servers_list;
}


sub updateServer {
	my ($server_id, $status) = @_;

	my $servers_id = production_db::updateServer($server_id, $status);
	
	return $servers_id;
}


1;