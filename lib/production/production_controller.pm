package production_controller;

use Template;
use Dancer2 appname => 'ProdDashboard';
use Data::Dumper;
use TryCatch;

use utils::ajax::response_helper;
use utils::support::action_helper;
use production::production_helper;
use production::production_logs_helper;

my $GREEN_COLOR = 1;
my $AMBER_COLOR = 2;
my $RED_COLOR = 3;

sub registerServers {
	my $error;

	try {
		my $post_params = params();
		my $server_id = $post_params->{'server_id'};
		my $status = $post_params->{'status'};

		my $registration_id = production_helper::registerServer($server_id, $status);

		return ajax_data_response(
			message => 'Successful.'
		);
	} catch($error){
		return handleException( $error );
	};
}

sub updateServers {
	my $error;

	try {
		my $server_id = param('server_id');
		my $status = param('status');
		my $message = param('msg');

		my $db_status = $AMBER_COLOR; # If status not received, set to AMBER initially.

		if(defined $status){
			if($status == 1){
				$db_status = $GREEN_COLOR;
			}else{
				$db_status = $RED_COLOR;
				production_logs_helper::addLogEntry($server_id, '', $message);
			}
		}

		my $updated_server = production_helper::updateServer($server_id, $db_status);

		return ajax_data_response(
			message => 'Successful.'
		);

	} catch($error){
		return handleException( $error );
	};
}

sub ragUpdate {
	my $error;

	try {
		my $servers_list = production_helper::getAllProdServers();
		my $current_time = time();

		foreach my $server (@$servers_list){
			# Update current server status
			serverUpdate( $server, $current_time );
		}

		return ajax_data_response(
			message => 'Successful.'
		);
	} catch($error){
		return handleException( $error );
	};
}

# Update server and set colour as per time passed
sub serverUpdate {
	my ($server, $current_time) = @_;

	my $amber_threshold = 3*60; # 3 mins
	my $error_threshold = 5*60; # 5 mins

	my $time_diff_in_sec = $current_time - $server->{last_active};

	if($time_diff_in_sec >= $amber_threshold && $time_diff_in_sec < $error_threshold){
		#update Amber status
		production_helper::updateServer($server->{server_number}, $AMBER_COLOR);
		production_logs_helper::addLogEntry($server->{server_number}, 'SERVER_TIMEOUT', 'Timeout Warning');
	}elsif($time_diff_in_sec > $error_threshold){
		#update Error status
		production_helper::updateServer($server->{server_number}, $RED_COLOR);
		production_logs_helper::addLogEntry($server->{server_number}, 'SERVER_TIMEOUT', 'Timeout Critical');
	}
}


1;