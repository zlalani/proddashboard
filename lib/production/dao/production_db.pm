package production_db;

use Dancer2 appname => 'ProdDashboard';
use Dancer2::Plugin::Database;
use TryCatch;
use Data::Dumper;

use errors::dbError;

sub registerServer {
	my ($server_id, $status) = @_;

	my $sth;
	my $ser_id;

	try {
		$sth = database->prepare("select * from register_prod_servers (?, ?)");
		$sth->execute( $server_id, $status );

		$ser_id  = $sth->fetchrow_array;

	} catch {
		errors::dbError->from_handle($sth, 'registerServer')->throw();
	}

	return $ser_id;
}

sub updateServer {
	my ($server_id, $status) = @_;

	my $sth;
	my $ser_id;

	try {
		$sth = database->prepare("select * from register_prod_servers (?, ?)");
		$sth->execute( $server_id, $status );

		$ser_id  = $sth->fetchrow_array;

	} catch {
		errors::dbError->from_handle($sth, 'updateServer')->throw();
	}

	return $ser_id;
}

sub getAllProdServers{
	my $sth;
	my $servers_list;

	try {
		$sth = database->prepare("select * from get_all_prod_servers ()");
		$sth->execute();

		$servers_list  = $sth->fetchall_arrayref({});

	} catch {
		errors::dbError->from_handle($sth, 'getAllProdServers')->throw();
	}

	return $servers_list;
}

sub addLogEntry{
	my ($server_num, $activity, $message) = @_;

	my $sth;
	my $log_record_id;

	try {
		$sth = database->prepare("select * from create_prod_log_entry (?, ?, ?)");
		$sth->execute( $server_num, $activity, $message );

		$log_record_id = $sth->fetchrow_array;

	} catch {
		errors::dbError->from_handle($sth, 'addLogEntry')->throw();
	}

	return $log_record_id;
}


1;