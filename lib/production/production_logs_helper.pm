package production_logs_helper;

use Template;
use Dancer2 appname => 'dashboard';
use Data::Dumper;
use TryCatch;
use production::dao::production_db;

sub addLogEntry {
	my ($server_number, $activity, $message) = @_;

	my $record_id = production_db::addLogEntry($server_number, $activity, $message);
	
	return $record_id;
}


1;
