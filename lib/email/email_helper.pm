package email_helper;

use Dancer2 appname => 'ProdDashboard';
use utils::Mailer;
use Data::Dumper;
use production::production_logs_helper;

# sends email
sub sendMail{
	my ($fromAddress, $toemailAddresses, $emailSubject, $emailBody) = @_;

	if( $fromAddress eq "" ){
		production_logs_helper::addLogEntry(0, 'MAIL ISSUE', 'Issue in sending status email. From address missing.');
		return 0;
	}

	if( $toemailAddresses eq "" ){
		production_logs_helper::addLogEntry(0, 'MAIL ISSUE', 'Issue in sending status email. To address missing.');
		return 0;
	}

	my $mailSuccessflag = Mailer::send_mail(
		$fromAddress,
		$toemailAddresses,
		$emailBody,
		$emailSubject
		);

	if(! $mailSuccessflag ){
		production_logs_helper::addLogEntry(0, 'MAIL ISSUE', 'Issue in sending status email.');
		return 0;
	}else{
		return 1;
	}
}
1;
