package errors::Error;

use strict;
use warnings;

use Moo;

with 'Throwable';

has 'message' => (
    is => 'ro',
    required => 1,
);

has 'target' => (
    is => 'ro'
);

use overload
    '""' => \&to_string;

sub to_string { $_[0]->message; }

1;
